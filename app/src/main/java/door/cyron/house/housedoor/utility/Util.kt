package door.cyron.house.housedoor.utility

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Geocoder
import android.net.Uri
import android.provider.Settings
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import door.cyron.house.housedoor.R
import door.cyron.house.housedoor.RoomsActivity
import door.cyron.house.housedoor.utility.Constant.Companion.ACCESS_FINE_LOCATION_PERMISSION
import door.cyron.house.housedoor.utility.Constant.Companion.MY_PERMISSIONS_REQUEST_LOCATION
import java.text.SimpleDateFormat
import java.util.*


class Util {

    companion object {


        fun getDateTime(currentDateTime: String): String {
            val currentDate = Date(currentDateTime.toLong())
            val df = SimpleDateFormat("MMM dd, yyyy HH:mm:ss")
            return df.format(currentDate)
        }


        fun isMyServiceRunning(serviceClass: Class<*>, activity: Activity): Boolean {
            val manager = activity!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.name == service.service.className) {
                    Log.i("isMyServiceRunning?", true.toString() + "")
                    return true
                }
            }
            Log.i("isMyServiceRunning?", false.toString() + "")
            return false
        }

        fun openWithChrome(context: Context, url: String) {
            val builder = CustomTabsIntent.Builder()
            if (context != null) {
                builder.setToolbarColor(ResourcesCompat.getColor(context.resources, R.color.colorPrimary, null))
            }
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(context, Uri.parse(url))
        }

        fun hideKeyboardFrom(view: View) {
            val imm = view.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun toast(activity: Activity, str: String) {
            Toast.makeText(activity, str, Toast.LENGTH_SHORT).show()
        }

        fun isLocationPermitted(activity: Activity): Boolean {
            val result = ActivityCompat.checkSelfPermission(activity, ACCESS_FINE_LOCATION_PERMISSION)
            return result == PackageManager.PERMISSION_GRANTED
        }

        fun isLocationPermitted(activity: Context): Boolean {
            val result = ActivityCompat.checkSelfPermission(activity, ACCESS_FINE_LOCATION_PERMISSION)
            return result == PackageManager.PERMISSION_GRANTED
        }

        fun showTextInputError(textInputLayout: TextInputLayout, message: String) {
            textInputLayout.editText!!.requestFocus()
            textInputLayout.isErrorEnabled = true
            textInputLayout.error = message
        }

        fun showSnackBar(view: View, message: String) {
            val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
            snackbar.show()
        }

        fun getDeviceId(context: Context): String {
            val device_uuid = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            return device_uuid ?: ""
        }

        fun showAlert(
            context: Context,
            cancelable: Boolean,
            messageResId: String,
            positiveMessageId: String,
            onClickListener: DialogInterface.OnClickListener
        ) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(context.getString(R.string.app_name))
            builder.setMessage(messageResId)
            builder.setCancelable(cancelable)
            builder.setPositiveButton(positiveMessageId, onClickListener)
            builder.create().show()
        }

        fun openAppSettingScreen(context: Activity) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", context.packageName, null)
            intent.data = uri
            context.startActivityForResult(intent, MY_PERMISSIONS_REQUEST_LOCATION)
        }


        fun convertPixelsToDp(px: Float, context: Context): Float {
            return px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }

//        fun createCustomMarker(context: Context, @DrawableRes resource: Int, _name: String): Bitmap {
//
//            val marker = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.custom_marker_layout, null)
//
//            val markerImage = marker.findViewById(R.id.user_dp) as CircleImageView
//            markerImage.setImageResource(resource)
//
//            val displayMetrics = DisplayMetrics()
//            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
//            marker.setLayoutParams(ViewGroup.LayoutParams(40, ViewGroup.LayoutParams.WRAP_CONTENT))
//            marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
//            marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
//            marker.buildDrawingCache()
//            val bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888)
//            val canvas = Canvas(bitmap)
//            marker.draw(canvas)
//
//            return bitmap
//        }

        fun createCustomMarker(context: Context): Bitmap {

            val marker = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                R.layout.own_location,
                null
            )

            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            marker.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.buildDrawingCache()
            val bitmap =
                Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            marker.draw(canvas)

            return bitmap
        }

        fun createCustomMarker(context: Context, color: String, _name: String): Bitmap {

            val marker = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                R.layout.custom_marker_layout,
                null
            )

            val markerImage = marker.findViewById(R.id.user_dp) as de.hdodenhof.circleimageview.CircleImageView
            markerImage.setBackgroundColor(Color.parseColor(color))

            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            marker.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.buildDrawingCache()
            val bitmap =
                Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            marker.draw(canvas)

            return bitmap
        }

        fun getAddress(activity: FragmentActivity?, latitude: Double, longitude: Double): String {
            var geocoder = Geocoder(activity, Locale.getDefault());

            var addresses = geocoder.getFromLocation(
                latitude,
                longitude,
                1
            ); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            var address = addresses.get(0)
                .getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            var city = addresses.get(0).getLocality();
            var state = addresses.get(0).getAdminArea();
            var country = addresses.get(0).getCountryName();
            var postalCode = addresses.get(0).getPostalCode();
            var knownName = addresses.get(0).getFeatureName();

            return address;

        }


        fun rotateImageDropdown(v: Float, v1: Float, view: View) {
            val animation = ObjectAnimator.ofFloat(view, "rotationX", v, v1)
            animation.duration = 150
            animation.repeatCount = 0
            animation.interpolator = AccelerateDecelerateInterpolator()
            animation.start()
        }

        fun startAnimationAccTwo(
            activity: RoomsActivity,
            viewTwoDemo: View,
            viewone: CircleImageView,
            viewTwo: CircleImageView,
            pos: Int,
            viewoneHeight: Int,
            viewoneWidth: Int,
            viewTwoX: Float,
            viewoneX: Float
        ) {

            exitAnimationTwo(activity, viewone, viewTwoX - viewoneX * 2, pos, viewTwoDemo)
            val animation = ObjectAnimator.ofFloat(
                viewTwo,
                "translationX", -(viewTwoX - (viewoneX + viewoneX))
            )
            animation.duration = 300

            animation.start()
            animation.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {

                }

                override fun onAnimationEnd(animator: Animator) {


                    viewTwo.layoutParams.height = viewoneHeight
                    viewTwo.layoutParams.width = viewoneWidth
                    viewTwo.requestLayout()
                }

                override fun onAnimationCancel(animator: Animator) {

                }

                override fun onAnimationRepeat(animator: Animator) {

                }
            })
        }


        fun exitAnimationTwo(activity: RoomsActivity, one: View, x: Float, pos: Int, viewTwoDemo: View) {
            val profileInSet = AnimatorSet()
            profileInSet.playTogether(
                ObjectAnimator.ofFloat(one, "alpha", 1f, 0f).setDuration(150),
                ObjectAnimator.ofFloat(one, "scaleX", 1f, 0.5f).setDuration(150),
                ObjectAnimator.ofFloat(one, "scaleY", 1f, 0.5f).setDuration(150)
            )
            profileInSet.start()
            profileInSet.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {

                }

                override fun onAnimationEnd(animator: Animator) {

                    viewTwoDemo.visibility = View.VISIBLE
                    startanimation(activity, viewTwoDemo, pos)

                }

                override fun onAnimationCancel(animator: Animator) {
                    viewTwoDemo.visibility = View.VISIBLE
                    startanimation(activity, viewTwoDemo, pos)
                }

                override fun onAnimationRepeat(animator: Animator) {

                }
            })

        }

        fun startanimation(activity: RoomsActivity, viewDemo: View, pos: Int) {
            viewDemo.visibility = View.VISIBLE
            val profileInSet = AnimatorSet()

            profileInSet.playTogether(
                ObjectAnimator.ofFloat(viewDemo, "alpha", 0f, 1f).setDuration(100),
                ObjectAnimator.ofFloat(viewDemo, "scaleX", 0.5f, 1f).setDuration(100),
                ObjectAnimator.ofFloat(viewDemo, "scaleY", 0.5f, 1f).setDuration(100)
            )
            profileInSet.start()
            profileInSet.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {

                }

                override fun onAnimationEnd(animator: Animator) {

                    (activity).drawerClose(pos)
                    //                updateFriendList(pos);
                }

                override fun onAnimationCancel(animator: Animator) {
                    //                updateFriendList(pos);
                    (activity).drawerClose(pos)
                }

                override fun onAnimationRepeat(animator: Animator) {

                }
            })
        }


        fun startAnimationAcc(
            activity: RoomsActivity,
            viewThreeDemo: View, viewone: CircleImageView, viewTwo: CircleImageView, pos: Int, viewoneHeight: Int
            , viewoneWidth: Int
        ) {

            exitAnimation(activity, viewone, viewTwo.x - viewone.x * 2, pos, viewThreeDemo)
            val animation = ObjectAnimator.ofFloat(
                viewTwo,
                "translationX", -(viewTwo.x - (viewone.x + viewone.x)).toInt() + 0.0f
            )
            animation.setDuration(300)

            animation.start()
            animation.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {

                }

                override fun onAnimationEnd(animator: Animator) {


                    viewTwo.layoutParams.height = viewoneHeight
                    viewTwo.layoutParams.width = viewoneWidth
                    viewTwo.requestLayout()
                }

                override fun onAnimationCancel(animator: Animator) {

                }

                override fun onAnimationRepeat(animator: Animator) {

                }
            })
        }

        fun exitAnimation(activity: RoomsActivity, one: View, x: Float, pos: Int, viewDemo: View) {
            val profileInSet = AnimatorSet()
            profileInSet.playTogether(
                ObjectAnimator.ofFloat(one, "alpha", 1f, 0f).setDuration(200),
                ObjectAnimator.ofFloat(one, "scaleX", 1f, 0.5f).setDuration(200),
                ObjectAnimator.ofFloat(one, "scaleY", 1f, 0.5f).setDuration(200)
            )
            profileInSet.start()
            profileInSet.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {

                }

                override fun onAnimationEnd(animator: Animator) {

                    viewDemo.visibility = View.VISIBLE
                    startanimation(activity, viewDemo, pos)

                }

                override fun onAnimationCancel(animator: Animator) {
                    viewDemo.visibility = View.VISIBLE
                    startanimation(activity, viewDemo, pos)
                }

                override fun onAnimationRepeat(animator: Animator) {

                }
            })

        }


    }


}
