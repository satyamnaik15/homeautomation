package door.cyron.house.housedoor.database;

import android.content.Context;
import android.util.Log;
import door.cyron.house.housedoor.database.entity.Trip;
import door.cyron.house.housedoor.utility.Util;

import java.util.List;


public class DbUtility {

    public static void startNewTrip(final String tripId, Context context) {
        /*{"tripId":"1111","vehicleId":"1122","timeDate":"123","status":"pending","loc":"kolkata","speed":"12","acc":"1"}
         */
        Log.d("DBUtility", "TRIP START called");
        final AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        new Thread(new Runnable() {
            public void run() {
                Trip trip = new Trip();
                trip.setTripId(tripId);
                trip.setVechicleId("KTM");
                trip.setStartTime("" + Util.Companion.getDateTime("" + System.currentTimeMillis()));
                trip.setStatus("START");
                appDatabase.getDao().insert(trip);
                Log.d("DBUtility", "TRIP STARTED");
            }
        }).start();

    }

    public static void updateTrip(final String tripId, final String data, final String extra,
                                  final Context context) {
        Log.d("DBUtility", "TRIP update called");
        final AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        new Thread(new Runnable() {
            public void run() {
                appDatabase.getDao().updateTrip(tripId, data, extra);
                Log.d("DBUtility", "TRIP updated");
            }
        }).start();

    }


    public static void updateAppendTrip(final String tripId, final String data, final String extra,
                                        final Context context) {
        Log.d("DBUtility", "TRIP updateAppendTrip called");
        final AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        new Thread(new Runnable() {
            public void run() {
                Trip trip = appDatabase.getDao().getTripData(tripId);
                Log.d("DBUtility", "TRIP updateAppendTriped " + trip.getData());
                if (trip.getData() == null && trip.getExtra() == null)
                    appDatabase.getDao().updateAppendTrip(tripId, data, extra);
                else if (trip.getData() == null && trip.getExtra() != null)
                    appDatabase.getDao().updateAppendTrip(tripId, data, trip.getExtra() + "," + extra);
                else
                    appDatabase.getDao().updateAppendTrip(tripId, trip.getData() + "," + data, trip.getExtra() + "," + extra);
            }
        }).start();

    }

    public static void endTrip(final String tripId, final String data,
                               final Context context) {
        Log.d("DBUtility", "TRIP END called");
        final AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        new Thread(new Runnable() {
            public void run() {
                Trip trip = appDatabase.getDao().getTripData(tripId);
                appDatabase.getDao().endTrip(tripId, "" + "" + Util.Companion.getDateTime("" + System.currentTimeMillis())
                        , "END", trip.getData() + "," + data);
                Log.d("DBUtility", "TRIP ENDED");
            }
        }).start();

    }

    public static List<Trip> getAllTrip(final Context context) {

        final AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        Log.d("DBUtility", "TRIP getAllTrip ");
        return appDatabase.getDao().getAllTrip();

    }

}
