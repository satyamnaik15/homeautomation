package door.cyron.house.housedoor.utility

import android.Manifest

class Constant {
    companion object {
        const val HOME_TITLE = "Home"
        const val TRIPS = "Trips"
        const val BIKE_MAP = "Bike"
        const val BULB = "Bulb"
        const val PACKAGE_NAME = "satyam.subham.uikit.login"

        const val GPS_REQUEST_CODE = 1000
        const val MY_PERMISSIONS_REQUEST_LOCATION = 99
        const val ACTIVITY_EXTRA = PACKAGE_NAME + ".ACTIVITY_EXTRA"
        const val LATEST_LATITUDE = PACKAGE_NAME + ".Latitude"
        const val LATEST_LONGITUDE = PACKAGE_NAME + ".Longitude"
        const val ACCESS_FINE_LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION

        const val BROADCAST_ACTION = "$PACKAGE_NAME.BROADCAST_ACTION"
    }

}