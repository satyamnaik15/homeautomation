package door.cyron.house.housedoor.trips.view


import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ViewSwitcher
import androidx.annotation.StyleRes
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import door.cyron.house.housedoor.R
import door.cyron.house.housedoor.callbacks.OnItemClickListener
import door.cyron.house.housedoor.database.DbUtility
import door.cyron.house.housedoor.databinding.FragmentTripsBinding
import door.cyron.house.housedoor.trips.model.HouseModel
import door.cyron.house.housedoor.trips.model.SwitchModel
import door.cyron.house.housedoor.trips.view.adapter.SliderAdapter
import door.cyron.house.housedoor.trips.view.cardSlider.CardSliderLayoutManager
import door.cyron.house.housedoor.trips.view.cardSlider.CardSnapHelper
import door.cyron.house.housedoor.utility.PreferenceManager
import door.cyron.house.housedoor.utility.Util
import java.lang.Double


/**
 * A simple [Fragment] subclass.
 *
 */
class TripsFragment : Fragment(), OnItemClickListener<SwitchModel>, OnMapReadyCallback {


    private val pics = intArrayOf(
        R.drawable.p1,
        R.drawable.p2,
        R.drawable.p3,
        R.drawable.p4,
        R.drawable.p5
    )
    private val descriptions =
        intArrayOf(
            R.string.text1,
            R.string.text2,
            R.string.text3,
            R.string.text4,
            R.string.text5
        )
    private val places = arrayOf("About", "About", "About", "About", "About")
    private val temperatures = arrayOf("21°C", "19°C", "17°C", "23°C", "20°C")

    private var houseModelArrayList: ArrayList<HouseModel>? = null
    private var listLatLang: ArrayList<LatLng>? = null
    private var sliderAdapter: SliderAdapter? = null

    private var layoutManger: CardSliderLayoutManager? = null
    private var countryOffset1: Int = 0
    private var countryOffset2: Int = 0
    private var countryAnimDuration: Long = 0
    private var currentPosition: Int = 0
    private var mMap: GoogleMap? = null
    private var mapFragment: SupportMapFragment? = null


    private lateinit var binding: FragmentTripsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        val view = inflater.inflate(R.layout.fragment_home, container, false)
        binding = FragmentTripsBinding.inflate(inflater, container, false)

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment!!.getMapAsync(this)
        setValue()
        initRecyclerView()
        initCountryText()
        initSwitchers()

        return binding.root
    }

    override fun onItemClick(t: SwitchModel, view: View, position: Int) {

    }

    private fun setValue() {
        houseModelArrayList = ArrayList()

        var listTrips = DbUtility.getAllTrip(activity)
        for (trip in listTrips) {
            val house1 = HouseModel()
            house1.pic = pics[0]
            house1.descriptions = getString(descriptions[0])

            house1.time = trip.startTime + " - " + trip.endTime
            val data = trip.data.split(",")
            var avgSpeed: Int = 0
            var count: Int = 0
            var highestSpeed: Int = 0
            var speedVal: Int = 0
            house1.place = "" + data.size
            for (eachSpeed in data) {

                if (eachSpeed != null && (eachSpeed.split(":")).size > 1) {
                    /* Log.d(
                         "TripsFragment",
                         "Speed--- " + (eachSpeed.split(":")[0] + " date time " + house1.time)
                     )*/
                    val speed = (eachSpeed.split(":")[0])
                    Log.d(
                        "TripsFragment",
                        "Speed---11 " + speed + " - " + highestSpeed + "loc" + (eachSpeed.split(":")[1])
                    )
                    try {
                        if (speed.isNotEmpty()) {
                            speedVal = speed.toInt()

                            if (speedVal > 0) {

                                avgSpeed += speedVal
                                count++

                                if (highestSpeed < speedVal) {
                                    highestSpeed = speedVal
                                    Log.d(
                                        "TripsFragment",
                                        "Speed--- HhighestSpeed after change " + highestSpeed + " speed " + speedVal
                                    )
                                }
                            }

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }
            Log.d(
                "TripsFragment",
                "Speed--- HhighestSpeed FINAl " + highestSpeed + " " + data.size
            )
            house1.name = "" + highestSpeed
            if (count > 0)
                house1.avgSpeed = "" + avgSpeed / count + " km/hr"
            else
                house1.avgSpeed = "0 km/hr"
            houseModelArrayList!!.add(house1)

        }

        sliderAdapter =
            SliderAdapter(houseModelArrayList!!, OnCardClickListener())
    }

    private fun initRecyclerView() {
        binding.recyclerView.adapter = sliderAdapter
        binding.recyclerView.setHasFixedSize(true)

        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    onActiveCardChange()
                }
            }
        })

        layoutManger = binding.recyclerView.layoutManager as CardSliderLayoutManager?

        CardSnapHelper().attachToRecyclerView(binding.recyclerView)
    }

    private fun initSwitchers() {
        if (houseModelArrayList!!.size == 0)
            return
        binding.tsTemperature.setFactory(TextViewFactory(R.style.TemperatureTextView, true))
        binding.tsTemperature.setCurrentText(houseModelArrayList!![0].avgSpeed)

        binding.tsPlace.setFactory(TextViewFactory(R.style.PlaceTextView, false))
        binding.tsPlace.setCurrentText(houseModelArrayList!![0].place)

        binding.tsClock.setFactory(TextViewFactory(R.style.ClockTextView, false))
        binding.tsClock.setCurrentText(houseModelArrayList!![0].time)

        binding.tsDescription.setInAnimation(activity, android.R.anim.fade_in)
        binding.tsDescription.setOutAnimation(activity, android.R.anim.fade_out)
        binding.tsDescription.setFactory(TextViewFactory(R.style.DescriptionTextView, false))
        binding.tsDescription.setCurrentText(houseModelArrayList!![0].descriptions)


    }

    private fun initCountryText() {
        countryAnimDuration = resources.getInteger(R.integer.labels_animation_duration).toLong()
        countryOffset1 = resources.getDimensionPixelSize(R.dimen.left_offset)
        countryOffset2 = resources.getDimensionPixelSize(R.dimen.card_width)

        binding.tvCountry1.x = countryOffset1.toFloat()
        binding.tvCountry2.x = countryOffset2.toFloat()
        if (houseModelArrayList!!.size > 0)
            binding.tvCountry1.text = houseModelArrayList!![0].name
        binding.tvCountry2.alpha = 0f

        binding.tvCountry1.typeface = Typeface.createFromAsset(activity!!.assets, "open-sans-extrabold.ttf")
        binding.tvCountry2.typeface = Typeface.createFromAsset(activity!!.assets, "open-sans-extrabold.ttf")
    }

    private fun setCountryText(text: String, left2right: Boolean) {
        val invisibleText: TextView
        val visibleText: TextView
        if (binding.tvCountry1.alpha > binding.tvCountry2.alpha) {
            visibleText = binding.tvCountry1
            invisibleText = binding.tvCountry2
        } else {
            visibleText = binding.tvCountry2
            invisibleText = binding.tvCountry1
        }

        val vOffset: Int
        if (left2right) {
            invisibleText.x = 0f
            vOffset = countryOffset2
        } else {
            invisibleText.x = countryOffset2.toFloat()
            vOffset = 0
        }

        invisibleText.text = text

        val iAlpha = ObjectAnimator.ofFloat(invisibleText, "alpha", 1f)
        val vAlpha = ObjectAnimator.ofFloat(visibleText, "alpha", 0f)
        val iX = ObjectAnimator.ofFloat(invisibleText, "x", countryOffset1 + 0f)
        val vX = ObjectAnimator.ofFloat(visibleText, "x", vOffset + 0f)

        val animSet = AnimatorSet()
        animSet.playTogether(iAlpha, vAlpha, iX, vX)
        animSet.duration = countryAnimDuration
        animSet.start()
    }

    private fun onActiveCardChange() {
        val pos = layoutManger!!.activeCardPosition
        if (pos == RecyclerView.NO_POSITION || pos == currentPosition) {
            return
        }

        onActiveCardChange(pos)
    }

    private fun onActiveCardChange(pos: Int) {
        val animH = intArrayOf(
            R.anim.slide_in_right,
            R.anim.slide_out_left
        )
        val animV = intArrayOf(
            R.anim.slide_in_top,
            R.anim.slide_out_bottom
        )

        val left2right = pos < currentPosition
        if (left2right) {
            animH[0] = R.anim.slide_in_left
            animH[1] = R.anim.slide_out_right

            animV[0] = R.anim.slide_in_bottom
            animV[1] = R.anim.slide_out_top
        }

        setCountryText(houseModelArrayList!!.get(pos % houseModelArrayList!!.size).name!!, left2right)

        binding.tsTemperature.setInAnimation(activity, animH[0])
        binding.tsTemperature.setOutAnimation(activity, animH[1])
        binding.tsTemperature.setText(houseModelArrayList!![pos % houseModelArrayList!!.size].avgSpeed)

        binding.tsPlace.setInAnimation(activity, animV[0])
        binding.tsPlace.setOutAnimation(activity, animV[1])
        binding.tsPlace.setText(houseModelArrayList!![pos % houseModelArrayList!!.size].place)

        binding.tsClock.setInAnimation(activity, animV[0])
        binding.tsClock.setOutAnimation(activity, animV[1])
        binding.tsClock.setText(houseModelArrayList!![pos % houseModelArrayList!!.size].time)

        binding.tsDescription.setText(houseModelArrayList!![pos % houseModelArrayList!!.size].descriptions)

        currentPosition = pos
        drawLineInmap(pos)
    }

    private fun drawLineInmap(pos: Int) {

        mMap!!.clear()
        var pref = PreferenceManager(activity)
        if (pref.locationLat.isNotEmpty()) {
            val ownLoc = LatLng(Double.parseDouble(pref.locationLat), Double.parseDouble(pref.locationLon))
            mMap?.moveCamera(CameraUpdateFactory.newLatLng(ownLoc))
            setOwnMarker(ownLoc)
            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(ownLoc, 13f))
        }
        var listTrips = DbUtility.getAllTrip(activity)

        val list = listTrips.get(pos).data.split(",")
        listLatLang = ArrayList()
        for (eachLoc in list) {
            if (eachLoc != null && mMap != null && (eachLoc.split(":")).size > 1) {
                /* Log.d(
                     "TripsFragment",
                     "Location--- " + (eachLoc.split(":")[1])
                 )*/
                if (eachLoc.split(":")[0].isNotEmpty() && (eachLoc.split(":")[0].toInt() > 0)) {
                    val lat = (eachLoc.split(":")[1]).split("-")[0]
                    val lon = (eachLoc.split(":")[1]).split("-")[1]

                    listLatLang!!.add(LatLng(lat.toDouble(), lon.toDouble()))
//                    mMap!!.addMarker(MarkerOptions()!!.position(LatLng(lat.toDouble(), lon.toDouble()))).title =
//                        eachLoc.split(":")[0]
                }
            }

        }
        val pattern = listOf(
            Dot(), Gap(20F), Dash(30F), Gap(20F)
        )
        if (listLatLang != null && mMap != null && listLatLang!!.size > 0) {

            mMap!!.addPolyline(
                (PolylineOptions())
                    .clickable(true)
                    .addAll(listLatLang)
                    .pattern(pattern)
            )

        }


    }

    private inner class TextViewFactory internal constructor(
        @param:StyleRes @field:StyleRes
        internal val styleId: Int, internal val center: Boolean
    ) : ViewSwitcher.ViewFactory {

        override fun makeView(): View {
            val textView = TextView(activity)

            if (center) {
                textView.gravity = Gravity.CENTER
            }

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                textView.setTextAppearance(activity, styleId)
            } else {
                textView.setTextAppearance(styleId)
            }

            return textView
        }

    }

    private inner class OnCardClickListener : View.OnClickListener {
        override fun onClick(view: View) {
            val lm = binding.recyclerView.layoutManager as CardSliderLayoutManager?

            if (lm!!.isSmoothScrolling) {
                return
            }

            val activeCardPosition = lm.activeCardPosition
            if (activeCardPosition == RecyclerView.NO_POSITION) {
                return
            }

            val clickedPosition = binding.recyclerView.getChildAdapterPosition(view)
            if (clickedPosition == activeCardPosition) {
            } else if (clickedPosition > activeCardPosition) {
                binding.recyclerView.smoothScrollToPosition(clickedPosition)
                onActiveCardChange(clickedPosition)
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap.uiSettings.isZoomControlsEnabled = true
//        googleMap.isMyLocationEnabled = true
        /* var pref = PreferenceManager(activity)
         if (pref.locationLat.isNotEmpty()) {
             val ownLoc = LatLng(Double.parseDouble(pref.locationLat), Double.parseDouble(pref.locationLon))
             mMap?.moveCamera(CameraUpdateFactory.newLatLng(ownLoc))
             setOwnMarker(ownLoc)
             mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(ownLoc, 15f))
         }*/
        drawLineInmap(0)
        /* for (trip in listTrips) {
             val list = trip.data.split(",")
             listLatLang = ArrayList()
             for (eachLoc in list) {
                 if (eachLoc != null && mMap != null && (eachLoc.split(":")).size > 1) {
                     Log.d(
                         "TripsFragment",
                         "Location--- " + (eachLoc.split(":")[1])
                     )
                     val lat = (eachLoc.split(":")[1]).split("-")[0]
                     val lon = (eachLoc.split(":")[1]).split("-")[1]

                     listLatLang!!.add(LatLng(lat.toDouble(), lon.toDouble()))

                 }

             }
             if (listLatLang != null && mMap != null && listLatLang!!.size > 0)
                 mMap!!.addPolyline(
                     (PolylineOptions())
                         .clickable(true)
                         .addAll(listLatLang)
                 )

         }
        */ /* var listTrips = DbUtility.getAllTrip(activity)

         for (trip in listTrips) {
             Log.d(
                 "TripsFragment",
                 "Trip details tripId ${trip.tripId} starttime ${trip.startTime} endTime ${trip.endTime} status ${trip.status} " +
                         "data ${trip.data}"
             )

         }*/
    }

    private fun setOwnMarker(ownLoc: LatLng) {
        mMap?.addMarker(
            MarkerOptions().position(ownLoc).icon(
                BitmapDescriptorFactory.fromBitmap(
                    activity?.let { Util.createCustomMarker(it) }
                )
            )
        )
//        myMarker?.setTag("OWN_LOCATION")
    }

}
