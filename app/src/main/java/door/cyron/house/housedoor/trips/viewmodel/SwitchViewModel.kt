package door.cyron.house.housedoor.trips.viewmodel

import door.cyron.house.housedoor.trips.model.SwitchModel

class SwitchViewModel(private val offer: SwitchModel) {


    val name: String?
        get() = offer.name

    val title: String?
        get() = offer.title

    val type: String?
        get() = offer.type


}
