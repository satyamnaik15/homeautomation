package door.cyron.house.housedoor

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.drawerlayout.widget.DrawerLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sohrab.obd.reader.application.ObdPreferences
import com.sohrab.obd.reader.constants.DefineObdReader
import com.sohrab.obd.reader.service.ObdReaderService
import com.sohrab.obd.reader.trip.TripRecord
import door.cyron.house.housedoor.bike.BikeFragment
import door.cyron.house.housedoor.bulb.FragmentBulb
import door.cyron.house.housedoor.callbacks.OnItemClickListener
import door.cyron.house.housedoor.database.DbUtility
import door.cyron.house.housedoor.drawer.NavigationDrawerAdapter
import door.cyron.house.housedoor.drawer.NavigationDrawerModel
import door.cyron.house.housedoor.trips.view.TripsFragment
import door.cyron.house.housedoor.utility.Constant
import door.cyron.house.housedoor.utility.Constant.Companion.BIKE_MAP
import door.cyron.house.housedoor.utility.Constant.Companion.BULB
import door.cyron.house.housedoor.utility.Constant.Companion.HOME_TITLE
import door.cyron.house.housedoor.utility.Constant.Companion.TRIPS
import door.cyron.house.housedoor.utility.PreferenceManager
import door.cyron.house.housedoor.utility.Util
import test.cyron.nmschool.nmschool.utility.FragmentHelper
import test.cyron.nmschool.nmschool.utility.FragmentHelper.getFragment
import java.util.*


class RoomsActivity : LocationBaseClass(), OnItemClickListener<NavigationDrawerModel> {


    private var adapter: NavigationDrawerAdapter? = null

    private val listItems = ArrayList<NavigationDrawerModel>()
    private val listProfile = ArrayList<NavigationDrawerModel>()
    private var drawer: DrawerLayout? = null
    private var drawerList: RecyclerView? = null
    var selectedDrawerPos = 1
    var mBroadcastReceiver: LocationServiceBroadCastReceiver? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawerList = findViewById<View>(R.id.left_drawer) as RecyclerView
        addNavValue()
        setNav(0)
        bikeMap()
        drawer?.addDrawerListener(object : DrawerLayout.DrawerListener {

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                //Called when a drawer's position changes.
                Log.e("TAG", "-----> $slideOffset")
            }

            override fun onDrawerOpened(drawerView: View) {
                //Called when a drawer has settled in a completely open state.
                //The drawer is interactive at this point.
                // If you have 2 drawers (left and right) you can distinguish
                // them by using id of the drawerView. int id = drawerView.getId();
                // id will be your layout's id: for example R.id.left_drawer
            }

            override fun onDrawerClosed(drawerView: View) {
                // Called when a drawer has settled in a completely closed state.
            }

            override fun onDrawerStateChanged(newState: Int) {
                // Called when the drawer motion state changes. The new state will be one of STATE_IDLE, STATE_DRAGGING or STATE_SETTLING.
            }
        })
        registerObd()
    }

    fun registerObd() {
        val service = ObdReaderService()
        if (!Util.isMyServiceRunning(service::class.java, this)) {

            val intentFilter = IntentFilter();
            intentFilter.addAction(DefineObdReader.ACTION_READ_OBD_REAL_TIME_DATA);
            intentFilter.addAction(DefineObdReader.ACTION_OBD_CONNECTION_STATUS);
            registerReceiver(mObdReaderReceiver, intentFilter);

            //start service which will execute in background for connecting and execute command until you stop
            startService(Intent(this, ObdReaderService::class.java))
        }
    }

    fun setBulbFrag() {
        FragmentHelper.clearBackStack(this)
        val fragment = FragmentBulb()
        FragmentHelper.replaceFragment(this@RoomsActivity, R.id.rooms_container, fragment, true, HOME_TITLE)

    }

    fun openTripsFrag() {
        FragmentHelper.clearBackStack(this)
        val fragment = TripsFragment()
        FragmentHelper.replaceFragment(this@RoomsActivity, R.id.rooms_container, fragment, false, HOME_TITLE)

    }

    fun bikeMap() {
        FragmentHelper.clearBackStack(this)
        val fragment = BikeFragment()
        FragmentHelper.replaceFragment(this@RoomsActivity, R.id.rooms_container, fragment, false, BIKE_MAP)

    }

    private fun addNavValue() {

        listItems.clear()
        listProfile.clear()

        val one = NavigationDrawerModel()
        one.name = ""
        one.image = 0
        listItems.add(one)

        val two = NavigationDrawerModel()
        two.name = BIKE_MAP
        two.image = R.drawable.green_dot_small
        listItems.add(two)

        val thr = NavigationDrawerModel()
        thr.name = TRIPS
        thr.image = R.drawable.green_dot_small
        listItems.add(thr)


        val six = NavigationDrawerModel()
        six.name = BULB
        six.image = R.drawable.green_dot_small
        listItems.add(six)

        val twoo = NavigationDrawerModel()
        twoo.name = "one"
        twoo.image = R.drawable.p3
        listProfile.add(twoo)

        val thrr = NavigationDrawerModel()
        thrr.name = "two"
        thrr.image = R.drawable.p1
        listProfile.add(thrr)

        val thrrr = NavigationDrawerModel()
        thrrr.name = "three"
        thrrr.image = R.drawable.p2
        listProfile.add(thrrr)


        val thrrr2 = NavigationDrawerModel()
        thrrr2.name = "four"
        thrrr2.image = R.drawable.p2
        listProfile.add(thrrr2)


        val thrrr3 = NavigationDrawerModel()
        thrrr3.name = "five"
        thrrr3.image = R.drawable.p2
        listProfile.add(thrrr3)


    }

    private fun updateNavigationDrawer() {

        drawerList?.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        drawerList?.layoutManager = layoutManager
        adapter = NavigationDrawerAdapter(this@RoomsActivity, this, selectedDrawerPos)
        drawerList?.adapter = adapter
    }

    private fun setNav(pos: Int) {

        var clickedItem = listProfile[pos]
        var zeroPosItem = listProfile[0]

        listProfile.removeAt(0)
        listProfile.add(0, clickedItem)
        listProfile.removeAt(pos)
        listProfile.add(pos, zeroPosItem)//swap clicked item

        updateNavigationDrawer()

        adapter?.setProfileDetails(listProfile)
        adapter?.setDrawerList(listItems)
    }

    override fun onItemClick(item: NavigationDrawerModel, view: View, position: Int) {

        Toast.makeText(this, "" + item.name, Toast.LENGTH_SHORT).show()
        selectedDrawerPos = position;
        var frag = FragmentHelper.getFragment(this, R.id.rooms_container)

        if (item.name.equals(BIKE_MAP)) {

            if (frag != null && !frag.tag.equals(BIKE_MAP)) {
                bikeMap()
            }
        } else if (item.name.equals(BULB)) {
            if (frag != null && !frag.tag.equals(BULB))
                setBulbFrag()
        } else {
            if (frag != null && !frag.tag.equals(TRIPS))
                openTripsFrag()
        }
        drawer!!.closeDrawer(Gravity.LEFT)

    }

    override fun onLocationReceived(lat: String, log: String) {
        Log.d("RoomsActivity", "onLocationReceived")
        var pref: PreferenceManager = PreferenceManager(this)
        pref.setLocation(lat, log)
        val frag = getFragment(this@RoomsActivity, BIKE_MAP)
        Log.d("RoomsActivity", "frag " + frag + "currentTripDetail " + pref.currentTripDetail)

        if (pref.currentTripDetail.isNotEmpty() && pref.locationLat.isNotEmpty()) {
            DbUtility.updateAppendTrip(
                pref.currentTripDetail,
                "" + ":" + pref.locationLat + "-" + pref.locationLon,
                "",
                this@RoomsActivity
            )
        }
        if (frag != null)
            (frag as BikeFragment).onLocationReceived(lat, log, true)
    }

    override fun onLocationError(error: String) {
        (getFragment(this@RoomsActivity, BIKE_MAP) as BikeFragment).onError(error)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish();
    }

    fun drawerClose(pos: Int) {
//        drawer?.closeDrawer(Gravity.LEFT)
        setNav(pos)
    }

    private val mObdReaderReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            val action = intent.action

            if (action == DefineObdReader.ACTION_OBD_CONNECTION_STATUS) {

                val connectionStatusMsg = intent.getStringExtra(ObdReaderService.INTENT_OBD_EXTRA_DATA)
                Log.d("RoomsActivity", "OBD $connectionStatusMsg")
//                Toast.makeText(this@RoomsActivity, connectionStatusMsg, Toast.LENGTH_SHORT).show()

                if (connectionStatusMsg == getString(R.string.obd_connected)) {
                    //OBD connected  do what want after OBD connection
                    Toast.makeText(this@RoomsActivity, connectionStatusMsg, Toast.LENGTH_SHORT).show()

                } else if (connectionStatusMsg == getString(R.string.connect_lost)) {
                    //OBD disconnected  do what want after OBD disconnection
                } else {
                    // here you could check OBD connection and pairing status
                }

            } else if (action == DefineObdReader.ACTION_READ_OBD_REAL_TIME_DATA) {

                val tripRecord = intent.getSerializableExtra("trip") as TripRecord ?: return
                if (intent.getStringExtra("type") != null && intent.getStringExtra("type").equals(
                        "speed",
                        ignoreCase = true
                    )
                ) {
                    Log.d("RoomsActivity", "OBD SPEED ---> " + tripRecord.speed + " - ")
                    var pref = PreferenceManager(this@RoomsActivity)
                    if (pref.currentTripDetail.isNotEmpty()) {
                        DbUtility.updateAppendTrip(
                            pref.currentTripDetail,
                            "" + tripRecord.speed + ":" + pref.locationLat + "-" + pref.locationLon,
                            "",
                            this@RoomsActivity
                        )
                    }

                }
                if (intent.getStringExtra("type") != null && intent.getStringExtra("type").equals(
                        "throttle",
                        ignoreCase = true
                    )
                ) {
                    Log.d("RoomsActivity", "OBD throttle ---> " + tripRecord.getmThrottlePos() + " - ")
                    var pref = PreferenceManager(this@RoomsActivity)
                    if (pref.currentTripDetail.isNotEmpty()) {
                        DbUtility.updateAppendTrip(
                            pref.currentTripDetail,
                            "" + "" + ":" + pref.locationLat + "-" + pref.locationLon,
                            tripRecord.getmThrottlePos(),
                            this@RoomsActivity
                        )
                    }

                }

                //                mObdInfoTextView.append("TYPE  - " + intent.getStringExtra("type") + "\n");
                //                mObdInfoTextView.setText("  - " + tripRecord.toString());
                //                mObdInfoTextView.append(tripRecord.toString());
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    fun unregisterObd() {
        try {
            unregisterReceiver(mObdReaderReceiver)
        } catch (e: Exception) {
        }
        //stop service
        stopService(Intent(this, ObdReaderService::class.java))
        // This will stop background thread if any running immediately.
        ObdPreferences.get(this).serviceRunningStatus = false
    }

    override fun onDestroy() {
        super.onDestroy()
        //unregister receiver
        unregisterObd()
    }

    fun unRegisterReceiver() {
        mBroadcastReceiver?.let { LocalBroadcastManager.getInstance(this).unregisterReceiver(it) }
    }

    fun registerReceiver() {
        if (mBroadcastReceiver == null)
            mBroadcastReceiver = LocationServiceBroadCastReceiver()
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mBroadcastReceiver!!,
            IntentFilter(Constant.BROADCAST_ACTION)
        )
    }


    inner class LocationServiceBroadCastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            Log.d(
                "RoomsActivity",
                "LocationServiceBroadCastReceiver ${intent.getStringExtra(Constant.LATEST_LATITUDE)}"
            )
            onLocationReceived(
                intent.getStringExtra(Constant.LATEST_LATITUDE),
                intent.getStringExtra(Constant.LATEST_LONGITUDE)
            )

        }
    }

}

