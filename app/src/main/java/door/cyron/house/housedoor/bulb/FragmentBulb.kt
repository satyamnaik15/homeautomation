package door.cyron.house.housedoor.bulb


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import door.cyron.house.housedoor.databinding.FragmentBulbBinding


/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentBulb : Fragment(), BulbViewModel.BulbInterface {
    override fun onProgress(status: Boolean) {

        if (status)
            binding.progress.visibility = View.VISIBLE
        else {
            binding.progress.visibility = View.GONE
        }
    }

    override fun onSucess(status: String) {
        binding.btnSwitch.visibility = View.VISIBLE
        binding.btnSwitch.setText(status)
        Log.d("FragmentBuld", "onSucess $status")
    }

    override fun onError(error: String, errorDetail: String) {
        Log.d("FragmentBuld", "onError $error  errorDetail $errorDetail")
    }

    private lateinit var binding: FragmentBulbBinding
    private lateinit var bulbViewModel: BulbViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentBulbBinding.inflate(inflater, container, false)
        bulbViewModel = BulbViewModel(this)
        binding.btnSwitch.setOnClickListener {
            bulbViewModel.updateStatus(binding.btnSwitch.text.toString())

        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.btnSwitch.visibility = View.GONE
        bulbViewModel.getBulbStatus()

    }

}
