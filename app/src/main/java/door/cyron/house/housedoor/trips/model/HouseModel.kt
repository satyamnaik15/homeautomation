package door.cyron.house.housedoor.trips.model

class HouseModel {
    var name: String? = null
    var place: String? = null
    var avgSpeed: String? = null
    var descriptions: String? = null
    var time: String? = null
    var pic: Int = 0
}
