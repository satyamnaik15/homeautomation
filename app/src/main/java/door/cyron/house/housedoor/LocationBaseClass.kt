package door.cyron.house.housedoor


import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import door.cyron.house.housedoor.utility.Constant.Companion.ACCESS_FINE_LOCATION_PERMISSION
import door.cyron.house.housedoor.utility.Constant.Companion.GPS_REQUEST_CODE
import door.cyron.house.housedoor.utility.Constant.Companion.MY_PERMISSIONS_REQUEST_LOCATION
import door.cyron.house.housedoor.utility.Util


/**
 * Created by Satyam Kumar Naik on 12/12/2017.
 */

abstract class LocationBaseClass : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    private var locationRequest: LocationRequest? = null
    private var googleApiClient: GoogleApiClient? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationCallback: LocationCallback? = null
    private var builder: LocationSettingsRequest.Builder? = null
    private var result: Task<LocationSettingsResponse>? = null
    private var forOnce = true

    abstract fun onLocationReceived(lat: String, log: String)

    abstract fun onLocationError(error: String)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("LocationBase", "onCreate")

        stopLocationUpdates()//stop if already called
        setLocationRequest()
    }

    fun setLocationRequest() {
        Log.d("LocationBase", "setLocationRequest")
        if (fusedLocationClient == null)
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (locationRequest == null) {
            locationRequest = LocationRequest.create()
//            locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest!!.interval = 2000
            locationRequest!!.fastestInterval = 1000
            builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest!!)
            result = LocationServices.getSettingsClient(this@LocationBaseClass).checkLocationSettings(builder!!.build())
        }
    }

    fun getLocation() {
        Log.d("LocationBase", "getLocation")
        if (Util.isLocationPermitted(this@LocationBaseClass)) {
            getLastLocation()
        } else {
            requestLocationPermission()
        }
    }

    fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_FINE_LOCATION_PERMISSION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            Util.showAlert(
                this,
                false,
                "Need permission",
                "enable",
                DialogInterface.OnClickListener { dialog, which -> Util.openAppSettingScreen(this@LocationBaseClass) })
        } else
            ActivityCompat.requestPermissions(
                this,
                arrayOf(ACCESS_FINE_LOCATION_PERMISSION),
                MY_PERMISSIONS_REQUEST_LOCATION
            )
    }


    private fun getLastLocation() {
        Log.d("LocationBase", "getLastLocation")
        stopLocationUpdates()
        setLocationRequest()
        buildGoogleApiClient()
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
//                var pref:PreferenceManager
//                pref!!.setLocation(locationResult!!.lastLocation.latitude,locationResult!!.lastLocation.latitude)
                onLocationReceived(
                    java.lang.Double.toString(locationResult!!.lastLocation.latitude),
                    java.lang.Double.toString(locationResult.lastLocation.longitude)
                )
                Log.d("LocationBase", "OnLOcation received!!!!!!")
                if (forOnce) {
                    stopLocationUpdates()
                }

            }
        }
    }

    @Synchronized
    private fun buildGoogleApiClient() {
        Log.d("LocationBase", "buildGoogleApiClient")
        if (googleApiClient == null)
            googleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        if (googleApiClient != null && !googleApiClient!!.isConnected)
            googleApiClient!!.connect()
    }

    override fun onConnected(bundle: Bundle?) {
        Log.d("LocationBase", "onConnected")

        result!!.addOnCompleteListener { task ->
            try {

                val response = task.getResult(ApiException::class.java)
                // All location settings are satisfied. The client can initialize location
                // requests here.
                if (ActivityCompat.checkSelfPermission(
                        this@LocationBaseClass,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {

                    fusedLocationClient!!.requestLocationUpdates(
                        locationRequest,
                        mLocationCallback!!, Looper.myLooper()
                    )
                }

            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            val resolvable = exception as ResolvableApiException
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(
                                this@LocationBaseClass,
                                GPS_REQUEST_CODE
                            )
                        } catch (e: IntentSender.SendIntentException) {
                            // Ignore the error.
                            onLocationError(e.toString())
                        } catch (e: ClassCastException) {
                            // Ignore, should be an impossible error.
                            onLocationError(e.toString())
                        }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        onLocationError("LOCATION SETTING UNAVAILABLE")
                }
            }
        }

    }

    override fun onConnectionSuspended(i: Int) {
        onLocationError("ON CONNECTION SUSPENDED")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

        onLocationError("ON CONNECTION FAILED")
    }

    @SuppressLint("MissingPermission")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("LocationBase", "onActivityResult requestCode $requestCode resultCode $resultCode")
        when (requestCode) {
            GPS_REQUEST_CODE -> when (resultCode) {
                Activity.RESULT_OK ->
                    //after clicking ok on resume will get called and there we are requesting for location
                    if (ActivityCompat.checkSelfPermission(
                            this@LocationBaseClass,
                            android.Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {

                        fusedLocationClient!!.requestLocationUpdates(
                            locationRequest,
                            mLocationCallback!!, Looper.myLooper()
                        )
                    }
                Activity.RESULT_CANCELED -> {
                    Toast.makeText(this, "Need GPS", Toast.LENGTH_SHORT).show()
                    finish()
                }
                else -> {
                }
            }
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                Toast.makeText(this, "Got IT !!!!", Toast.LENGTH_SHORT).show()
                getLocation()
            }
        }

    }

    fun stopLocationUpdates() {
        Log.d("LocationBase", "stopLocationUpdates")

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        if (fusedLocationClient != null && mLocationCallback != null)
            fusedLocationClient!!.removeLocationUpdates(mLocationCallback!!)
        if (googleApiClient != null && googleApiClient!!.isConnected) {
            googleApiClient!!.disconnect()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLastLocation()
                } else {
                    //                    if (shouldShowLocationPopUp) {
                    //                        Utility.showAlert(LocationBaseClass.this,false, "Need permission", "enable", new DialogInterface.OnClickListener() {
                    //                            @Override
                    //                            public void onClick(DialogInterface dialog, int which) {
                    //                                Utility.openAppSettingScreen(LocationBaseClass.this);
                    //                            }
                    //                        });
                    //                        shouldShowLocationPopUp = false;
                    //                    } else {
                    Toast.makeText(this, "Need permission", Toast.LENGTH_SHORT).show()
                    finish()
                    //                    }
                }

                return
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("LocationBase", "onDestroy")
        stopLocationUpdates()
    }
}
