package door.cyron.house.housedoor.bulb

import androidx.databinding.Bindable
import door.cyron.house.housedoor.BR
import door.cyron.house.housedoor.callbacks.Request
import door.cyron.house.housedoor.callbacks.ResponseListener
import door.cyron.house.housedoor.configuration.ViewModel
import door.cyron.house.housedoor.retrofit.RetrofitClient
import door.cyron.house.housedoor.retrofit.RetrofitRequest

class BulbViewModel(private val listener: BulbInterface) : ViewModel() {
    private var request: Request? = null
    private var empty: Boolean = false
    private var value: String = ""

    interface BulbInterface {
        fun onSucess(status: String)
        fun onProgress(status: Boolean)

        fun onError(error: String, detail: String)
    }


    @Bindable
    fun isEmpty(): Boolean {
        return empty
    }

    fun setEmpty(empty: Boolean) {
        this.empty = empty
        notifyPropertyChanged(BR.empty)
    }

    @Bindable
    fun getValue(): String {
        return value
    }

    fun setValue(value: String) {
        this.value = value
        notifyPropertyChanged(BR.value)
    }

    fun download() {
        //        setLoading(true);
        //        setLoading(false);
        //        offers = new ArrayList<>();
        //        setOffers(response.getOfferList());
    }

    fun getBulbStatus() {
        isLoading = true
        listener.onProgress(true)

        val call = RetrofitClient.getAPIInterface().getBulbStatus()
        request = RetrofitRequest(call, object : ResponseListener<BulbStatusResponseModel> {
            override fun onResponse(response: BulbStatusResponseModel?, headers: okhttp3.Headers?) {

                isLoading = false
                listener.onProgress(false)

                if (response != null) {

                    if (response.resCode.equals("0000")) {
                        response.data!!.status?.let {
                            listener.onSucess(it)
//                            setValue(it)
                        }
                        return

                    }
                    listener.onError("" + response.resCode, "" + response.resStatus)
                    return

                }
                listener.onError("101", "Null response")

            }

            override fun onError(status: Int, errors: String) {
                isLoading = false
                listener.onProgress(false)
                listener.onError("" + status, errors)
            }

            override fun onFailure(throwable: Throwable) {
                isLoading = false
                isRetry = true
                listener.onProgress(false)
                listener.onError(throwable.toString(), "Failure")
            }
        })
        request!!.enqueue()
    }

    override fun retry() {
        //        super.retry();
        request!!.retry();
    }

    override fun clear() {
        request?.cancel()
    }

    fun updateStatus(status: String) {
        listener.onProgress(true)
        val ovj = BulbStatusUpdate()
        if (status.equals("On", true)) {
            ovj.status = "OFF"
        } else {
            ovj.status = "ON"
        }

        val call = RetrofitClient.getAPIInterface().updateBulbStatus(ovj)
        request = RetrofitRequest(call, object : ResponseListener<BulbStatusResponseModel> {
            override fun onResponse(response: BulbStatusResponseModel?, headers: okhttp3.Headers?) {

                isLoading = false
                listener.onProgress(false)

                if (response != null) {

                    if (response.resCode.equals("0000")) {
                        response.data!!.status?.let {
                            listener.onSucess(it)
//                            setValue(it)
                        }
                        return

                    }
                    listener.onError("" + response.resCode, "" + response.resStatus)
                    return

                }
                listener.onError("101", "Null response")

            }

            override fun onError(status: Int, errors: String) {
                isLoading = false
                listener.onProgress(false)
                listener.onError("" + status, errors)
            }

            override fun onFailure(throwable: Throwable) {
                isLoading = false
                isRetry = true
                listener.onProgress(false)
                listener.onError(throwable.toString(), "Failure")

            }
        })
        request!!.enqueue()

    }


}