package door.cyron.house.housedoor.utility;

import android.content.Context;
import android.content.SharedPreferences;
import kotlin.jvm.Synchronized;


public class PreferenceManager {

    private final static String FORTIS_PREFERENCES = "cyron_saloon";
    private final static String FIREBASE_TOKEN = "firebase_token";
    private final static String LOCATION_LAT = "location_lat";
    private final static String TRIP_DETAILS = "trip_details";
    private final static String LOCATION_LON = "location_lon";


    private SharedPreferences sharedPreferences;

    private PreferenceManager() {

    }

    public PreferenceManager(Context context) {
        this();
        sharedPreferences = context.getApplicationContext().getSharedPreferences(FORTIS_PREFERENCES, Context.MODE_PRIVATE);
    }


    public void setFirebaseToken(String token) {
        sharedPreferences.edit().putString(FIREBASE_TOKEN, token).apply();
    }

    public String getFirebaseToken() {
        String token = sharedPreferences.getString(FIREBASE_TOKEN, "");
        return token;
    }


    public void setLocation(String lat, String lon) {
        sharedPreferences.edit().putString(LOCATION_LAT, lat).apply();
        sharedPreferences.edit().putString(LOCATION_LON, lon).apply();

    }

    @Synchronized
    public String getLocationLat() {
        return sharedPreferences.getString(LOCATION_LAT, "");
    }

    @Synchronized
    public String getLocationLon() {
        return sharedPreferences.getString(LOCATION_LON, "");
    }

    public void setCurrentTripDetail(String tripDetails) {
        sharedPreferences.edit().putString(TRIP_DETAILS, tripDetails).apply();
    }

    public void clearCurrentTripDetail() {
        sharedPreferences.edit().putString(TRIP_DETAILS, "").apply();
    }

    public String getCurrentTripDetail() {
        return sharedPreferences.getString(TRIP_DETAILS, "");
    }

}
