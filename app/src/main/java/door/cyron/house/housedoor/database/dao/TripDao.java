package door.cyron.house.housedoor.database.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import door.cyron.house.housedoor.database.entity.Trip;

import java.util.List;

@Dao
public interface TripDao {

    /* @Query("SELECT COUNT(*) FROM notification where uri LIKE  :uri ")
     int isPresent(String uri);

     @Insert
     void insert(Notification... notifications);
 */
    @Insert
    void insert(Trip... trip);

    @Query("update Trip set data=:data" +
            ",extra=:extra where tripId in(:tripId)")
    long updateTrip(String tripId, String data, String extra);

    @Query("update Trip set data=:data" +
            ",extra=:extra where tripId in(:tripId)")
    long updateAppendTrip(String tripId, String data, String extra);

    @Query("SELECT * FROM Trip where tripId in(:tripId)")
    Trip getTripData(String tripId);

    @Query("update Trip set endTime=:endTime,status=:status,data=:data" +
            " where tripId in(:tripId)")
    long endTrip(String tripId, String endTime, String status, String data);

    @Query("SELECT * FROM Trip")
    List<Trip> getAllTrip();

}
    /*@Insert
    void updateTrip(Trip... trip);*/

