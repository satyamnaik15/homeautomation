package door.cyron.house.housedoor

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.sohrab.obd.reader.application.ObdPreferences
import com.sohrab.obd.reader.constants.DefineObdReader.ACTION_OBD_CONNECTION_STATUS
import com.sohrab.obd.reader.constants.DefineObdReader.ACTION_READ_OBD_REAL_TIME_DATA
import com.sohrab.obd.reader.obdCommand.ObdConfiguration
import com.sohrab.obd.reader.service.ObdReaderService
import com.sohrab.obd.reader.trip.TripRecord
import door.cyron.house.housedoor.bike.BikeFragment
import test.cyron.nmschool.nmschool.utility.FragmentHelper

class BikeActivity : AppCompatActivity() {
    lateinit var tvDetails: TextView
    lateinit var progress_bar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bike)
//        setHome()

        progress_bar = findViewById(R.id.progress_bar)
        tvDetails = findViewById(R.id.tvDetails)
        ObdConfiguration.setmObdCommands(this, null);


        val intentFilter = IntentFilter();
        intentFilter.addAction(ACTION_READ_OBD_REAL_TIME_DATA);
        intentFilter.addAction(ACTION_OBD_CONNECTION_STATUS);
        registerReceiver(mObdReaderReceiver, intentFilter);

        //start service which will execute in background for connecting and execute command until you stop
        startService(Intent(this, ObdReaderService::class.java))
//        setHome()
    }

    fun setHome() {
        FragmentHelper.clearBackStack(this)
        val fragment = BikeFragment()
        FragmentHelper.replaceFragment(this@BikeActivity, R.id.home_container, fragment, false, "BikeFrag")

    }

    private val mObdReaderReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            progress_bar.setVisibility(View.GONE)
            val action = intent.action

            if (action == ACTION_OBD_CONNECTION_STATUS) {

                val connectionStatusMsg = intent.getStringExtra(ObdReaderService.INTENT_OBD_EXTRA_DATA)
                tvDetails.setText(connectionStatusMsg)
                Toast.makeText(this@BikeActivity, connectionStatusMsg, Toast.LENGTH_SHORT).show()

                if (connectionStatusMsg == getString(R.string.obd_connected)) {
                    //OBD connected  do what want after OBD connection
                } else if (connectionStatusMsg == getString(R.string.connect_lost)) {
                    //OBD disconnected  do what want after OBD disconnection
                } else {
                    // here you could check OBD connection and pairing status
                }

            } else if (action == ACTION_READ_OBD_REAL_TIME_DATA) {

                val tripRecord = intent.getSerializableExtra("trip") as TripRecord ?: return
                if (intent.getStringExtra("type") != null && intent.getStringExtra("type").equals(
                        "speed",
                        ignoreCase = true
                    )
                ) {
                    tvDetails.setText("SPEED ---> " + tripRecord.speed + " - ")
                }

                //                mObdInfoTextView.append("TYPE  - " + intent.getStringExtra("type") + "\n");
                //                mObdInfoTextView.setText("  - " + tripRecord.toString());
                //                mObdInfoTextView.append(tripRecord.toString());
            }

        }
    }


    override fun onDestroy() {
        super.onDestroy()
        //unregister receiver
        unregisterReceiver(mObdReaderReceiver)
        //stop service
        stopService(Intent(this, ObdReaderService::class.java))
        // This will stop background thread if any running immediately.
        ObdPreferences.get(this).serviceRunningStatus = false
    }
}
