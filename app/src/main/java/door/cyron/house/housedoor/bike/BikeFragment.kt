package door.cyron.house.housedoor.bike


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startForegroundService
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import door.cyron.house.housedoor.R
import door.cyron.house.housedoor.database.DbUtility
import door.cyron.house.housedoor.databinding.FragmentBikeBinding
import door.cyron.house.housedoor.RoomsActivity
import door.cyron.house.housedoor.service.LocationService
import door.cyron.house.housedoor.utility.PreferenceManager
import door.cyron.house.housedoor.utility.Util
import door.cyron.house.housedoor.utility.Util.Companion.isMyServiceRunning
import java.lang.Double


/**
 * A simple [Fragment] subclass.
 *
 */
class BikeFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private val TAG = "BikeFragment : " + javaClass.simpleName

    private var mMap: GoogleMap? = null
    private var mapFragment: SupportMapFragment? = null
    private lateinit var binding: FragmentBikeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        val view = inflater.inflate(R.layout.fragment_home, container, false)
        binding = FragmentBikeBinding.inflate(inflater, container, false)

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment!!.getMapAsync(this)
        val mBackgroundSyncService = LocationService()
        binding.btnStartLocServic.setOnClickListener {
            //            activity!!.startService(Intent(activity, LocationService::class.java))
            val mServiceIntent = Intent(activity, LocationService::class.java)
            if (binding.btnStartLocServic.text.equals("START location")) {


                if (!activity?.let { it1 -> isMyServiceRunning(mBackgroundSyncService::class.java, it1) }!!) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        context?.let { startForegroundService(it, mServiceIntent) }
                    } else {
                        activity!!.startService(mServiceIntent)
                    }
                    (activity as RoomsActivity).registerReceiver()
                    binding.btnStartLocServic.text = "STOP location"

                    var pref = PreferenceManager(activity)
                    val tripId = Util.getDateTime("" + System.currentTimeMillis())
                    Log.d("BikeFragment", "START currentTripDetail $tripId")

                    pref.currentTripDetail = "" + tripId
                    DbUtility.startNewTrip("" + tripId, activity)
                    (activity as RoomsActivity).registerObd()
                }
            } else {
                var pref = PreferenceManager(activity)
                activity!!.stopService(mServiceIntent)
                Log.d("BikeFragment", "STOP currentTripDetail " + pref.currentTripDetail)

                DbUtility.endTrip(
                    "" + pref.currentTripDetail,
                    ":" + pref.locationLat + "-" + pref.locationLon,
                    activity
                )
                pref.clearCurrentTripDetail()
                (activity as RoomsActivity).unRegisterReceiver()
                (activity as RoomsActivity).unregisterObd()
                binding.btnStartLocServic.text = "START location"

            }

        }
        if (!activity?.let { isMyServiceRunning(mBackgroundSyncService::class.java, it) }!!) {
            binding.btnStartLocServic.text = "START location"
        } else {
            binding.btnStartLocServic.text = "STOP location"

        }

        return binding.root
    }


    fun onLocationReceived(lat: String, log: String, isOwnLoc: Boolean) {
        if (mapFragment != null || mMap != null) {
            Util.showSnackBar(mapFragment?.getView()!!, "Location Received")


            mMap?.clear()
            val ownLoc = LatLng(Double.parseDouble(lat), Double.parseDouble(log))
            mMap?.moveCamera(CameraUpdateFactory.newLatLng(ownLoc))
//                etLocation!!.setText(Util.getAddress(activity, ownLoc.latitude, ownLoc.longitude))
            setOwnMarker(ownLoc)
            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(ownLoc, 15f))
        }
    }

    private fun setOwnMarker(ownLoc: LatLng) {
        mMap?.addMarker(
            MarkerOptions().position(ownLoc).icon(
                BitmapDescriptorFactory.fromBitmap(
                    activity?.let { Util.createCustomMarker(it) }
                )
            )
        )
//        myMarker?.setTag("OWN_LOCATION")
    }

    fun onError(error: String) {
        Util.showSnackBar(mapFragment?.getView()!!, "Error $error")
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap.uiSettings.isZoomControlsEnabled = true
//        googleMap.isMyLocationEnabled = true
        var pref = PreferenceManager(activity)
        if (pref.locationLat.isNotEmpty()) {
            (activity as (RoomsActivity)).onLocationReceived(pref.locationLat, pref.locationLon)
        }
        (activity as (RoomsActivity)).getLocation()
        mMap?.setOnMarkerClickListener(this)

    }


    override fun onMarkerClick(marker: Marker): Boolean {

        Log.d(TAG, "onMarkerClick " + marker!!.position.latitude + " - " + marker!!.position.longitude)
        if (marker.tag != "OWN_LOCATION") {
            val latLng = LatLng(marker!!.position.latitude, marker!!.position.longitude)
            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
        }
        return false
    }


}
