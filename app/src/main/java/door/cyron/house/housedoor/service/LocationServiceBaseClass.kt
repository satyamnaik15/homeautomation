package door.cyron.house.housedoor.service


import android.annotation.TargetApi
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import door.cyron.house.housedoor.utility.Util


/**
 * Created by Satyam Kumar Naik on 12/12/2017.
 */

abstract class LocationServiceBaseClass : Service(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    private var locationRequest: LocationRequest? = null
    private var googleApiClient: GoogleApiClient? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationCallback: LocationCallback? = null
    private var builder: LocationSettingsRequest.Builder? = null
    private var result: Task<LocationSettingsResponse>? = null

    abstract fun onLocationReceived(lat: String, log: String)

    abstract fun onError(error: String)

    override fun onCreate() {
        super.onCreate()
        Log.d("LocationService", "onCreate ")
        setLocationRequest()
        buildGoogleApiClient()
        getLocation()
    }

    override fun onBind(p0: Intent?): IBinder? {
        Log.d("LocationService", "onBind ")

        return null
    }

    fun setLocationRequest() {
        Log.d("LocationService", "setLocationRequest")
        if (fusedLocationClient == null)
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (locationRequest == null) {
            locationRequest = LocationRequest.create()
            locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest!!.interval = 0
            locationRequest!!.fastestInterval = 0
            builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest!!)
            result = LocationServices.getSettingsClient(this@LocationServiceBaseClass)
                .checkLocationSettings(builder!!.build())
        }
    }

    fun getLocation() {
        if (Util.isLocationPermitted(this)) {
            Log.d("LocationService", "getLastLocation ")
            getLastLocation()
        } else {

            Log.d("LocationService", "need Location permission")
        }
    }

    private fun getLastLocation() {
//        stopLocationUpdates()
//        setLocationRequest()

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
//                var pref:PreferenceManager
//                pref!!.setLocation(locationResult!!.lastLocation.latitude,locationResult!!.lastLocation.latitude)
                Log.d("LocationService", "onLocationResult !!!!!!!! ")
                onLocationReceived(
                    java.lang.Double.toString(locationResult!!.lastLocation.latitude),
                    java.lang.Double.toString(locationResult.lastLocation.longitude)
                )


            }
        }
    }

    @Synchronized
    private fun buildGoogleApiClient() {
        if (googleApiClient == null)
            googleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        if (googleApiClient != null && !googleApiClient!!.isConnected)
            googleApiClient!!.connect()
    }


    override fun onConnectionSuspended(i: Int) {
        onError("ON CONNECTION SUSPENDED")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

        onError("ON CONNECTION FAILED")
    }

    fun stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        if (fusedLocationClient != null && mLocationCallback != null)
            fusedLocationClient!!.removeLocationUpdates(mLocationCallback!!)
        if (googleApiClient != null && googleApiClient!!.isConnected) {
            googleApiClient!!.disconnect()
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        stopLocationUpdates()
        Log.d("LocationService", "onDestroy !!!!!!!! ")

    }

    override fun onConnected(bundle: Bundle?) {
        Log.d("LocationService", "onConnected ")
        result!!.addOnCompleteListener { task ->
            try {

                val response = task.getResult(ApiException::class.java)
                // All location settings are satisfied. The client can initialize location
                // requests here.
                if (ActivityCompat.checkSelfPermission(
                        this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    Log.d("LocationService", "PERMISSION_GRANTED requestLocationUpdates")
                    createAndShowForegroundNotification(this, 9779)
                    fusedLocationClient!!.requestLocationUpdates(
                        locationRequest,
                        mLocationCallback!!, Looper.myLooper()
                    )
                }

            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        onError("ENABLE GPS")
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                        onError("LOCATION SETTING UNAVAILABLE")
                }
            }
        }

    }

    private fun createAndShowForegroundNotification(yourService: Service, notificationId: Int) {

        val builder = getNotificationBuilder(
            yourService,
            "979797", // Channel id
            NotificationManagerCompat.IMPORTANCE_LOW
        ) //Low importance prevent visual appearance for this notification channel on top
        builder.setOngoing(true)
            .setContentTitle("Bike App")
            .setContentText("Getting location ....")

        val notification = builder.build()

        yourService.startForeground(notificationId, notification)

        /* // Cancel previous notification
        final NotificationManager nm = (NotificationManager) yourService.getSystemService(Activity.NOTIFICATION_SERVICE);
        nm.cancel(lastShownNotificationId);*/
    }

    fun getNotificationBuilder(context: Context, channelId: String, importance: Int): NotificationCompat.Builder {
        val builder: NotificationCompat.Builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            prepareChannel(context, channelId, importance)
            builder = NotificationCompat.Builder(context, channelId)
        } else {
            builder = NotificationCompat.Builder(context)
        }
        return builder
    }

    @TargetApi(26)
    private fun prepareChannel(context: Context, id: String, importance: Int) {
        val nm = context.getSystemService(Activity.NOTIFICATION_SERVICE) as NotificationManager

        if (nm != null) {
            var nChannel: NotificationChannel? = nm.getNotificationChannel(id)
            if (nChannel == null) {
                nChannel = NotificationChannel(id, "Bike", importance)
                nChannel.setShowBadge(false)
                nm.createNotificationChannel(nChannel)
            }
        }
    }

}
