package door.cyron.house.housedoor.bulb

import com.google.gson.annotations.SerializedName


class BulbStatusResponseModel {

    @SerializedName("resStatus")
    var resStatus: String? = null
    @SerializedName("resCode")
    var resCode: String? = null
    @SerializedName("data")
    var data: Data? = null

    inner class Data {

        @SerializedName("_id")
        var id: String? = null
        @SerializedName("status")
        var status: String? = null
        @SerializedName("__v")
        var v: Int? = null

    }
}
