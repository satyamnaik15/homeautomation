package door.cyron.house.housedoor.acount


import door.cyron.house.housedoor.callbacks.Request
import door.cyron.house.housedoor.callbacks.ResponseListener
import door.cyron.house.housedoor.configuration.ViewModel
import door.cyron.house.housedoor.retrofit.RetrofitClient
import door.cyron.house.housedoor.retrofit.RetrofitRequest
import okhttp3.Headers
import retrofit2.Call


class SigninViewmodel(private val signinListener: SigninListener) : ViewModel() {
    private var request: Request? = null

    interface SigninListener {
        fun onSucess()

        fun onError(error: String)
    }

    fun signin(email: String, type: String, pass: String) {
        isLoading = true
        val signinModel = SigninModel()
        signinModel.email = email
        signinModel.password = pass
        signinModel.type = type
        val call = RetrofitClient.getAPIInterface().signIn(signinModel)
        request = RetrofitRequest(call, object : ResponseListener<SignInResponseModel> {
            override fun onResponse(response: SignInResponseModel, headers: Headers) {
                isLoading = false
                signinListener.onSucess()
            }

            override fun onError(status: Int, errors: String) {
                isLoading = false
                signinListener.onError(errors)
            }

            override fun onFailure(throwable: Throwable) {
                isLoading = false
                isRetry = true
                signinListener.onError("Failure")
            }
        })
        request!!.enqueue()
    }

    override fun retry() {
        super.retry()
        request!!.retry()
    }

    override fun clear() {
        if (request != null)
            request!!.cancel()
    }

}

