package door.cyron.house.housedoor.service

import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import door.cyron.house.housedoor.utility.Constant
import door.cyron.house.housedoor.utility.Constant.Companion.LATEST_LATITUDE
import door.cyron.house.housedoor.utility.Constant.Companion.LATEST_LONGITUDE

class LocationService : LocationServiceBaseClass() {

    override fun onLocationReceived(lat: String, log: String) {
        Log.d("LocationService", "onLocationReceived ")
        val localIntent = Intent(Constant.BROADCAST_ACTION)
        localIntent.putExtra(LATEST_LATITUDE, lat)
        localIntent.putExtra(LATEST_LONGITUDE, log)
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent)
    }

    override fun onError(error: String) {
        Log.d("LocationService", "onLocationError ")
    }


}
